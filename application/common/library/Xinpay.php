<?php

namespace app\common\library;

use app\common\model\XinpayModel;


/**
 * 鑫支付190125
 * Class Xinpay
 * @package app\common\library
 */
class Xinpay
{
    public function __construct()
    {
        $this->key = config('xinpayconfig.XIN_KEY');
        $this->mchno = config('xinpayconfig.XIN_MCHNO');
    }

    public function createOrder($data)
    {
        $key1 = $this->key;//平台商户的密钥 自填
        $model = new XinpayModel();
        $model->mchno = $this->mchno;//平台商户号 自填
        $model->price = $data['bpprice'] * 100;
        $model->bill_title = '会员充值';
        $model->bill_body = "会员充值";
        $model->pay_type = '7';
        $model->nonce_str = self::createNonceStr();//随机串
        $rawStr = json_decode(json_encode($model));
        $rawStr = self::object_array($rawStr);
        ksort($rawStr);

        $data = "";
        foreach ($rawStr as $key => $value) {
            if ($value != null) {
                $data .= $key . "=" . $value . "&";
            }
        }
        $data .= "key=" . $key1;
        $model->sign = strtoupper(md5($data));
        $result = self::post('http://api.5w2019.com/api/wypay/createOrder', $model);
        if ($result) {
            $result = json_decode($result, true);
            if ($result['resultCode'] == '200') {//支付订单创建成功
                return $result['order'];//对方系统的订单号，需要存下来校验用
            }
            return false;
        }
        return false;
    }


    function object_array($array)
    {
        if (is_object($array)) {
            $array = (array)$array;
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = self::object_array($value);
            }
        }
        return $array;
    }

    function getBytes($string)
    {
        $bytes = array();
        for ($i = 0; $i < strlen($string); $i++) {
            $bytes[] = ord($string[$i]);
        }
        return $bytes;
    }

    function post($url, $data)
    {
        $postdata = json_encode($data);
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'content-type: application/x-www-form-urlencoded;charset=UTF-8',
                'content' => $postdata
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    function createNonceStr($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

}