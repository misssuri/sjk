<?php
/**
 * Created by PhpStorm.
 * User: xiaohui_huang
 * Date: 2019/1/25
 * Time: 9:34
 */

namespace app\common\model;

class XinpayModel
{
    /// <summary>
    /// 商户号
    /// </summary>
    public $mchno;
    /// <summary>
    /// 支付类型
    /// </summary>
    public $pay_type;
    /// <summary>
    /// 金额
    /// </summary>
    public $price;
    /// <summary>
    /// 商品标题
    /// </summary>
    public $bill_title;
    /// <summary>
    /// 商品描述
    /// </summary>
    public $bill_body;
    /// <summary>
    /// 32位以内随机字符串，只包含字母和数字
    /// </summary>
    public $nonce_str;
    /// <summary>
    /// 透传参数（非必填）
    /// </summary>
    public $linkId;
    /// <summary>
    /// 签名
    /// </summary>
    public $sign;

}