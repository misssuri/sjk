<?php

namespace app\qq123adminhtgl\controller;

use think\Db;

/**
 * 系统设置和积分比例设置，可自定义设置
 */
class Setup extends Base
{

    /**
     * 基本设置
     * @author lukui  2017-04-19
     * @return [type] [description]
     */
    public function index()
    {

        $map['group'] = 1;
        $map['status'] = 1;

        $data = Db::name('config')->where($map)->order('sort asc')->select();
        $this->assign('data', $data);
        return $this->fetch();
    }


    /**
     * 比例设置
     * @author lukui  2017-04-19
     * @return [type] [description]
     */
    public function proportion()
    {

        $map['group'] = 2;
        $map['status'] = 1;

        $data = Db::name('config')->where($map)->order('sort asc')->select();
        $this->assign('data', $data);
        return $this->fetch('index');
    }


    /**
     * 图标设置
     * @author lukui  2017-04-19
     * @return [type] [description]
     */
    public function icoimg()
    {

        $map['group'] = 3;
        $map['status'] = 1;

        $data = Db::name('config')->where($map)->order('sort asc')->select();
        $this->assign('data', $data);
        return $this->fetch('index');
    }


    /**
     * 配置比例
     * @author lukui  2017-04-19
     * @return [type] [description]
     */
    public function addsetup()
    {

        if (input('post.')) {
            $data = input('post.');
            $data['create_time'] = $data['update_time'] = time();
            $data['status'] = 1;

            if (isset($data['id'])) {
                $ids = Db::name('config')->update($data);
            } else {
                $ids = Db::name('config')->insert($data);
            }

            if ($ids) {
                cache('conf', null);
                return WPreturn('配置成功', 1);
            } else {
                return WPreturn('配置失败，请重试', -1);
            }

            exit;
        } else {

            if (input('param.id')) {
                $id = input('param.id');
                $data = Db::name('config')->where('id', $id)->find();
                $this->assign($data);
            }
            return $this->fetch();
        }


    }


    /**
     * 编辑配置/比例
     * @author lukui  2017-04-19
     * @return [type] [description]
     */
    public function editconf()
    {

        if (input('post.')) {

            $data = input('post.');
            // file_put_contents( dirname(__FILE__).'/QTB____$data_____URL.txt', var_export( $data, true ), FILE_APPEND );
            foreach ($data as $k => $v) {
                $arr = explode('_', $k);
                $_data['id'] = $arr[1];
                $_data['value'] = $v;
                $file = request()->file('pic_' . $_data['id']);
                //file_put_contents( dirname(__FILE__).'/QTB____$file____URL.txt', var_export($file , true ), FILE_APPEND );
                if ($file) {

                    $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
                    if ($info) {
                        $_data['value'] = '/public' . DS . 'uploads/' . $info->getSaveName();
                    }
                }
                if ($_data['value'] == '' && isset($arr[2]) && $arr[2] == 3) {
                    continue;
                }

                Db::name('config')->update($_data);

            }
            cache('conf', null);
            $this->success('编辑成功');
        }


    }


    public function deleteconf()
    {

        if (input('post.')) {

            $id = input('post.id');

            if (!$id) {
                return WPreturn('参数错误', -1);
            }

            $_data['id'] = $id;
            $_data['status'] = 0;

            $ids = Db::name('config')->update($_data);
            if ($ids) {
                cache('conf', null);
                return WPreturn('删除成功', 1);
            } else {
                return WPreturn('删除失败，请重试', -1);
            }

        }
    }


    /**
     * 所有配置列表
     * @author lukui  2017-04-19
     * @return [type] [description]
     */
    public function deploy()
    {

        $map['status'] = 1;

        $data = Db::name('config')->where($map)->order('sort asc')->select();
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * 分润配置
     */
    public function fenrun()
    {

        $map['group'] = 4;
        $fenruns = db('config')->where($map)->order('sort asc')->select();

        $this->assign('fenruns', $fenruns);
        return $this->fetch();

    }

    /**
     * 删除分润配置
     */
    public function deletefenrun()
    {

        $id = input('id');
        $ids = db('config')->where('id', $id)->delete();
        if ($ids) {
            return WPreturn('删除成功', 1);
        } else {
            return WPreturn('删除失败', -1);
        }

    }

    /**
     * 编辑分润配置
     */
    public function editfenrun()
    {


        $post = input('post.');

        try {
            if (isset($post['id']) && !empty($post['id'])) {
                $ids = db('config')->update($post);
            } else {
                unset($post['id']);
                $post['type'] = 4;
                $post['group'] = 4;
                $post['create_time'] = $data['update_time'] = time();
                $post['status'] = 1;
                $ids = db('config')->insert($post);
            }
            if ($ids) {
                return WPreturn('操作成功', 1);
            } else {
                return WPreturn('操作失败', -1);
            }
        } catch (\Exception $e) {
            return WPreturn('操作失败', -1);
        }
    }

    /**客服设置
     * @return array|mixed|string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function service()
    {
        $map['name'] = 'web_service';
        $ws = Db::name('config')->where($map)->order('sort asc')->select();
        if (input('post.')) {
            $post = input('post.');
            $content = isset($post['content']) ? $post['content'] : '';;
            if ($ws) {
                Db::name('config')->where($map)->update(['value' => $content]);
            } else {
                Db::name('config')->insert([
                    'name' => 'web_service',
                    'type' => '5',
                    'title' => '客服设置',
                    'group' => '5',
                    'create_time' => time(),
                    'update_time' => time(),
                    'status' => '1',
                    'value' => $content,
                    'sort' => '1',
                ]);
            }
            return WPreturn('设置成功', 1);
        }
        $content = '';
        if ($ws) {
            $content = $ws[0]['value'];
        }
        $this->view->assign('content', $content);
        return $this->fetch();
    }

    /**
     * 佣金说明
     * @return array|false|mixed|string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function yjsm()
    {
        $map['name'] = 'yjsm';
        $yjsm = Db::name('config')->where($map)->order('sort asc')->select();
        if (input('post.')) {
            $post = input('post.');
            $content = isset($post['content']) ? $post['content'] : '';;
            if ($yjsm) {
                Db::name('config')->where($map)->update(['value' => $content]);
            } else {
                Db::name('config')->insert([
                    'name' => 'yjsm',
                    'type' => '6',
                    'title' => '佣金说明',
                    'group' => '6',
                    'create_time' => time(),
                    'update_time' => time(),
                    'status' => '1',
                    'value' => $content,
                    'sort' => '1',
                ]);
            }
            return WPreturn('设置成功', 1);
        }

        $content = '';
        if ($yjsm) {
            $content = $yjsm[0]['value'];
        }
        $this->view->assign('content', $content);
        return $this->fetch();
    }


}
