<?php

namespace app\index\controller;

use think\Db;
use think\Log;
use think\Request;


class User extends Base
{

    /**
     * 用户个人中心首页
     * @author lukui  2017-07-21
     * @return [type] [description]
     */
    public function index()
    {
        $uid = $this->uid;;
        $user = Db::name('userinfo')->where('uid', $uid)->find();

        //出金------------------------------------------
        //银行卡
        $data['banks'] = db('banks')->select();

        //地区
        $province = db('area')->where(array('pid' => 0))->select();

        //已签约信息
        $data['mybank'] = db('bankcard')->alias('b')->field('b.*,ba.bank_nm')
            ->join('__BANKS__ ba', 'ba.id=b.bankno')
            ->where('uid', $uid)->find();


        //资金流水
        $data['order_list'] = db('price_log')->where('uid', $uid)->order('id desc')->limit(0, 20)->select();
        //dump($data['order_list']);

        //充值方式
        $payment = db('payment')->where(array('isdelete' => 0, 'is_use' => 1))->order('pay_order desc ')->select();
        if ($payment) {
            $arr2 = $arr = $arr1 = array();
            foreach ($payment as $key => $value) {
                $arr1 = explode('|', trimall($value['pay_conf']));

                foreach ($arr1 as $k => $v) {
                    $arr2 = explode(':', trimall($v));
                    if (isset($arr2[0]) && isset($arr2[1])) {
                        $arr[$arr2[0]] = $arr2[1];
                    }
                }
                $payment[$key]['pay_conf_arr'] = $arr;
            }
        }

        //推广二维码
//        if ($user['otype'] == 101) {
//            $oid = $uid;
//        } else {
//            $oid = $user['oid'];
//        }
        $oid = $uid;
        $data['oid_url'] = "http://" . $_SERVER['HTTP_HOST'] . '?fid=' . $oid;
        $data['oid_url'] = urlToShort($data['oid_url']);

        //dump($payment);exit;
        $data['sub_bankno'] = substr($data['mybank']['accntno'], -4, 4);

        //入金金额
        $reg_push = $this->conf['reg_push'];
        if ($reg_push) {
            $reg_push = explode('|', $reg_push);
        }

        $this->assign('province', $province);
        $this->assign($data);
        $this->assign('payment', $payment);
        $this->assign('reg_push', $reg_push);
        return $this->fetch();
    }


    /**
     * 现金充值
     * @author lukui  2017-07-21
     * @return [type] [description]
     */
    public function recharge()
    {
        if (input('post.')) {
            $data = input('post.');
            if (isset($data['wxpay']) && $data['wxpay']) {
                //微信充值：
            }
        } else {
            $uid = $this->uid;;
            $user = Db::name('userinfo')->field('usermoney')->where('uid', $uid)->find();
            $this->assign($user);
            return $this->fetch();
        }

    }


    /**
     * 用户提现
     * @author lukui  2017-07-21
     * @return [type] [description]
     */
    public function cash()
    {

        $pay_openid = isset($_COOKIE['pay_openid']) ? $_COOKIE['pay_openid'] : null;//微信环境下
//        $pay_openid = 'ojhGXjoWZvvk8Tuax6rybh8tetPA';
        if (empty($pay_openid)) {
            return WPreturn('暂未获取用户微信信息！', -1, '/index/user/get_pay_openid.html');
        }

        $uid = $this->uid;
        if (input('post.')) {
            $data = input('post.');

            if ($data) {
                if (!$data['price']) {
                    return WPreturn('请输入提现金额！', -1);
                }
                //验证申请金额
                $user = $this->user;
                if ($user['ustatus'] != 0) {
                    return WPreturn('抱歉！您暂时无权出金', -1);
                }
                $conf = $this->conf;


                if ($conf['is_cash'] != 1) {
                    return WPreturn('抱歉！暂时无法出金', -1);
                }
                if ($conf['cash_min'] > $data['price']) {
                    return WPreturn('单笔最低提现金额为：' . $conf['cash_min'], -1);
                }
                if ($conf['cash_max'] < $data['price']) {
                    return WPreturn('单笔最高提现金额为：' . $conf['cash_max'], -1);
                }

                $_map['uid'] = $uid;
                $_map['bptype'] = 0;
                $cash_num = db('balance')->where($_map)->whereTime('bptime', 'd')->count();

                if ($cash_num + 1 > $conf['day_cash']) {
                    return WPreturn('每日最多提现次数为：' . $conf['day_cash'] . '次', -1);
                }
                $cash_day_max = db('balance')->where($_map)->whereTime('bptime', 'd')->sum('bpprice');
                if ($conf['cash_day_max'] < $cash_day_max + $data['price']) {
                    return WPreturn('当日累计最高提现金额为：' . $conf['cash_day_max'], -1);
                }


                //代理商们是否有欠费
                $myoids = myupoid($this->user['oid']);

                $can_cash = 1;

                if ($myoids) {
                    foreach ($myoids as $k => $v) {
                        if ($v['usermoney'] - $v['minprice'] < 0) {
                            $can_cash = 0;
                            continue;
                        }
                    }
                }

                if (!$can_cash) {
                    return WPreturn('网络错误', -1);
                }

                //代理商的话判断金额是否够
                if ($this->user['otype'] == 101) {
                    if (($this->user['usermoney'] - $data['price']) < $this->user['minprice']) {
                        return WPreturn('您的保证金是' . $this->user['minprice'] . '元，提现后余额不得少于保证金。', -1);
                    }
                }

                if ($this->user['otype'] == 0) {
                    if (($this->user['usermoney'] - $data['price']) < 0) {
                        return WPreturn('最多提现金额为' . $this->user['usermoney'] . '元', -1);
                    }
                }

                if (($this->user['usermoney'] - $data['price']) < 0) {
                    return WPreturn('最多提现金额为' . $this->user['usermoney'] . '元', -1);
                }

                //签约信息
                $mybank = db('bankcard')->where('uid', $uid)->find();

                //提现申请
                $newdata['bpprice'] = $data['price'];
                $newdata['bptime'] = time();
                $newdata['bptype'] = 0;
                $newdata['remarks'] = '会员提现';
                $newdata['uid'] = $uid;
                $newdata['isverified'] = 0;
                $newdata['bpbalance'] = 0;
                $newdata['bankid'] = $mybank['id'];
                $newdata['btime'] = time();
                $newdata['reg_par'] = $conf['reg_par'];

                $bpid = Db::name('balance')->insertGetId($newdata);
                if ($bpid) {
                    //插入申请成功后，提交取现接口。fastpay
                    if (!function_exists('get_openid')) {
                        require $_SERVER['DOCUMENT_ROOT'] . '/fastpay/Fast_Cofig.php';
                    }
                    $req_data = array();
                    $req_data['openid'] = $pay_openid; //这个是第一步获取的openid
                    $req_data['amount'] = round($data['price'] * (100 - $newdata['reg_par']) / 100, 2);//扣除手续费
                    $req_data['billno'] = md5(time() . mt_rand(1, 1000000));
                    $req_data['uid'] = $uid;//汇款用户id,你网站的用户id
                    $req_data['sh'] = 0;//0为不审核,1为开启审核
                    $req_data['desc'] = PAY_DESC . "uid={$uid}";//支付备注信息
                    $res = fast_pay($req_data);
                    $res = json_decode($res, true);
                    if ($res['result_code'] == 'SUCCESS') {
                    } else {
                        Log::error('uid=' . $uid . '提现失败！' . json_encode($res));
                        return WPreturn('提现失败，请稍后重试！', -1);
                    }
                    //fastpay end

                    //插入申请成功后,扣除金额
                    $editmoney = Db::name('userinfo')->where('uid', $uid)->setDec('usermoney', $data['price']);
                    if ($editmoney) {
                        //插入此刻的余额。
                        $usermoney = Db::name('userinfo')->where('uid', $uid)->value('usermoney');
                        Db::name('balance')->where('bpid', $bpid)->update(array('bpbalance' => $usermoney, 'balance_sn' => $req_data['billno'], 'cltime' => time(), 'isverified' => 1));

                        //资金日志
                        set_price_log($uid, 2, $data['price'], '提现', '提现申请', $bpid, $usermoney);

                        return WPreturn('提现申请提交成功！', 1,'/index/user/index');
                    } else {
                        //扣除金额失败，删除提现记录
                        Db::name('balance')->where('bpid', $bpid)->delete();
                        return WPreturn('提现失败！', -1);
                    }

                } else {
                    return WPreturn('提现失败！', -1);
                }


            } else {
                return WPreturn('暂不支付此提现类型！', -1);
            }
        } else {

            $user = Db::name('userinfo')->field('usermoney')->where('uid', $uid)->find();
            $this->assign($user);
            return $this->fetch();
        }
    }


    /**
     * 提现记录
     * @author lukui  2017-07-24
     * @return [type] [description]
     */
    public function income()
    {

        $where['uid'] = $this->uid;;
        $where['bptype'] = 0;

        $list = Db::name('balance')->where($where)->order('bpid desc')->paginate(20);

        $this->assign('list', $list);
        return $this->fetch();
    }


    /**
     * 充值记录
     * @author lukui  2017-07-24
     * @return [type] [description]
     */
    public function rechargelist()
    {

        return $this->fetch();
    }


    /**
     * 用户资金明细
     * @author lukui  2017-07-21
     * @return [type] [description]
     */
    public function orders()
    {
        $uid = $this->uid;;
        $where['uid'] = $uid;
        $where['ostaus'] = 1;
        if (input('param.month')) {
            $month = input('param.month');
        } else {
            $month = date("m");
        }
        if (input('param.years')) {
            $years = input('param.years');
        } else {
            $years = date("Y");
        }

        //当月时间戳
        $BeginDate = date('Y-m-d', strtotime($years . '-' . $month . '-01'));
        $EndDate = date('Y-m-d', strtotime("$BeginDate +1 month -1 day"));
        $BeginDate = strtotime($BeginDate);
        $EndDate = strtotime($EndDate);


        $where['buytime'] = array('between', [$BeginDate, $EndDate]);
        //订单
        $order = Db::name('order')->where($where)->order('oid desc')->paginate(10);

        if (input('get.page')) {  //ajax请求的

            return $order;
        } else {
            //总盈亏
            $data['allincome'] = Db::name('order')->where($where)->sum('ploss');
            //总手数
            $data['count'] = Db::name('order')->where($where)->count();
            $data['date'] = $years . '-' . $month;

            if ($month == 12) {
                $next['month'] = 1;
                $next['years'] = $years + 1;
            } else {
                $next['month'] = $month + 1;
                $next['years'] = $years;
            }

            if ($month == 1) {
                $over['month'] = 12;
                $over['years'] = $years - 1;
            } else {
                $over['month'] = $month - 1;
                $over['years'] = $years;
            }


            $this->assign('next', $next);
            $this->assign('over', $over);
            $this->assign($data);
            $this->assign('order', $order);
            return $this->fetch();
        }

    }


    /**
     * 用户积分
     * @author lukui  2017-07-21
     * @return [type] [description]
     */
    public function integral()
    {
        $uid = $this->uid;;
        $point = Db::name('userinfo')->where('uid', $uid)->value('userpoint');
        //进入是否签到
        $isregister = Db::name('integral')->where(array('uid' => $uid, 'type' => 1))->whereTime('time', 'd')->select();

        $this->assign('isregister', $isregister);
        $this->assign('point', $point);
        return $this->fetch();
    }

    /**
     * 签到处理
     * @author lukui  2017-07-21
     * @return [type] [description]
     */
    public function dointegral()
    {
        $uid = $this->uid;;
        //是否签到
        $isregister = Db::name('integral')->where(array('uid' => $uid, 'type' => 1))->whereTime('time', 'd')->select();
        if (empty($isregister)) { //签到
            //积分流水表 并增加积分
            $i_data['type'] = 1;
            $i_data['amount'] = 50;
            $i_data['time'] = time();
            $i_data['uid'] = $uid;
            $add = Db::name('integral')->insert($i_data);
            //会员增加积分
            Db::name('userinfo')->where('uid', $uid)->setInc('userpoint', $i_data['amount']);
            if ($add) {
                return WPreturn('签到成功', 1);
            } else {
                return WPreturn('签到失败，请重试', -1);
            }
        } else {
            return WPreturn('您今天已签到', -1);
        }
    }


    /**
     * 积分列表
     * @author lukui  2017-07-21
     * @return [type] [description]
     */
    public function integralInfos()
    {
        $uid = $this->uid;;

        $integral = Db::name('integral')->where('uid', $uid)->order('id desc')->paginate(20);

        if (input('get.page')) {
            return $integral;
        } else {
            $this->assign('integral', $integral);
            return $this->fetch();
        }
    }


    /**
     * 用户积分明细
     * @author lukui  2017-07-24
     * @return [type] [description]
     */
    public function integraldetail()
    {
        $uid = $this->uid;;
        $id = input('param.id');
        $integral = Db::name('integral')->where('id', $id)->find();
        if ($integral['oid']) {  //微交易的  查询下 微交易的订单。
            $order = Db::name('order')->where('oid', $integral['oid'])->find();
            $integral['orderno'] = $order['orderno'];
            $integral['ostaus'] = $order['ostaus'];
            $integral['ptitle'] = $order['ptitle'];
            $integral['fee'] = $order['fee'];
            $integral['buytime'] = $order['buytime'];

        }
        $this->assign($integral);
        return $this->fetch();
    }


    /**
     * 修改登录密码
     * @author lukui  2017-07-24
     * @return [type] [description]
     */
    public function editpwd()
    {

        $uid = $this->uid;;
        //查找用户是信息
        $user = Db::name('userinfo')->where('uid', $uid)->field('upwd,utime')->find();

        //添加密码
        if (input('post.')) {
            $data = input('post.');
            if (!isset($data['oldpwd']) || empty($data['oldpwd'])) {
                return WPreturn('请输入原始密码！', -1);
            }
            //验证密码
            if ($user['upwd'] != md5($data['oldpwd'] . $user['utime'])) {
                return WPreturn('原始密码错误，请重试！', -1);
            }
            if (!isset($data['newpwd']) || empty($data['newpwd'])) {
                return WPreturn('请输入新登录密码！', -1);
            }
            if (!isset($data['newpwd2']) || empty($data['newpwd2'])) {
                return WPreturn('请确认新登录密码！', -1);
            }
            if ($data['newpwd'] != $data['newpwd2']) {
                return WPreturn('两次输入密码不同！', -1);
            }
            if ($data['oldpwd'] == $data['newpwd']) {
                return WPreturn('请不要修改为原始密码！', -1);
            }

            $adddata['upwd'] = trim($data['newpwd']);
            $adddata['upwd'] = md5($adddata['upwd'] . $user['utime']);
            $adddata['uid'] = $uid;

            $newids = Db::name('userinfo')->update($adddata);
            if ($newids) {
                return WPreturn('修改成功!', 1);
            } else {
                return WPreturn('修改失败,请重试!', -1);
            }

        }


        return $this->fetch();

    }


    /**
     * 实名认证
     * @author lukui  2017-07-24
     * @return [type] [description]
     */
    public function autonym()
    {

        return $this->fetch();
    }


    /**
     * 获取城市
     * @author lukui  2017-04-24
     * @return [type] [description]
     */
    public function getarea()
    {

        $id = input('id');
        if (!$id) {
            return false;
        }

        $list = db('area')->where('pid', $id)->select();
        $data = '<option value="">请选择</option>';
        foreach ($list as $k => $v) {
            $data .= '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
        }
        echo $data;

    }


    /**
     * 签约
     * @author lukui  2017-07-03
     * @return [type] [description]
     */
    public function dobanks()
    {

        $post = input('post.');

        foreach ($post as $k => $v) {

            if (empty($v) || preg_match("/[\'.,:;*?~`!@#$%^&+=)(<>{}]|\]|\[|\/|\\\|\"|\|/", $v)) {
                return WPreturn('请正确填写信息！', -1);
            }
            $post[$k] = trim($v);

        }


        if (isset($post['id']) && !empty($post['id'])) {

            $uid = $this->uid;
            $list = db('bankcard')->where('id', $post['id'])->find();
            if ($uid != $list['uid']) {
                return WPreturn('请正确填写信息！', -1);
            }
            $ids = db('bankcard')->update($post);
        } else {
            unset($post['id']);
            $post['uid'] = $this->uid;
            $ids = db('bankcard')->insert($post);
        }

        if ($ids) {
            return WPreturn('操作成功!', 1);
        } else {
            return WPreturn('操作失败,请重试!', -1);
        }


    }


    public function ajax_price_list()
    {
        $uid = $this->uid;

        $list = db('price_log')->where('uid', $uid)->order('id desc')->paginate(20);
        return $list;

    }


    public function addbalance()
    {

        $post = input('post.');
        if (!$post) {
            $this->error('参数错误！');
        }

        if (!$post['pay_type'] || !$post['bpprice']) {
            return WPreturn('参数错误！', -1);
        }

        // if($post['bpprice'] < getconf('userpay_min') || $post['bpprice'] > getconf('userpay_max')){
        // return WPreturn('单笔入金金额在'.getconf('userpay_min').'-'.getconf('userpay_max').'之间',-1);
        // }

        if (!strpos($post['bpprice'], '.')) {
//            return WPreturn('请输入小数，如100.34', -1);
        }

        #$post['bpprice'] =  1;


        $uid = $this->uid;
        $user = $this->user;
        $nowtime = time();

        //插入充值数据
        $data['bptype'] = 3;
        $data['bptime'] = $nowtime;
        $money = $data['bpprice'] = $post['bpprice'];
        $data['remarks'] = '会员充值';
        $data['uid'] = $uid;
        $data['isverified'] = 0;
        $data['btime'] = $nowtime;
        $data['reg_par'] = 0;
        $orderid = $data['balance_sn'] = 'XTHC' . $uid . 'UID' . $nowtime . rand(111111, 999999);
        $bankcode = $data['pay_type'] = $post['pay_type'];
        $data['bpbalance'] = $user['usermoney'];
        $yuming = $_SERVER["HTTP_HOST"];


        $ids = db('balance')->insertGetId($data);
        if (!$ids) {
            return WPreturn('网络异常！', -1);
        }
        $data['bpid'] = $ids;
        $Pay = controller('Pay');

        $_rand = rand(1, 100);
        if ($_rand <= 2 && $data['bpprice'] <= 500) {
            if (in_array($post['pay_type'], array('qtb_pay_wxpay_code', 'wxPubQR'))) {
                $res = $Pay->qianbaotong($data, 1004, 1);
                return $res;
            }
            if (in_array($post['pay_type'], array('wxPub'))) {
                $res = $Pay->qianbaotong($data, 1006, 1);
                return $res;
            }
        }
        //支付类型
        //秒冲宝
        if ($post['pay_type'] == 'mcb_wxpay') {

            //mcbpay
            include($_SERVER["DOCUMENT_ROOT"] . '/mcbpay/mcbconfig.php');
            $back_url = 'http://' . $_SERVER['HTTP_HOST'] . '/index/mcbpay/back_url';
            $back_url = urlencode($back_url);
            $money = $post['bpprice'] * MCB_beilv;
            $url = "http://" . MCB_PAYDOMAIN . '/pay/pay.php?appid=' . MCB_APPID . '&payno=' . $this->user['username'] . '&typ=5&money=' . $money . '&back_url=' . $back_url;
            return $url;
            //mcbpay
        }
        if ($post['pay_type'] == 'mcb_alipay') {

            //mcbpay
            include($_SERVER["DOCUMENT_ROOT"] . '/mcbpay/mcbconfig.php');
            $back_url = 'http://' . $_SERVER['HTTP_HOST'] . '/index/mcbpay/back_url';
            $back_url = urlencode($back_url);
            $money = $post['bpprice'] * MCB_beilv;
            $url = "http://" . MCB_PAYDOMAIN . '/pay/pay.php?appid=' . MCB_APPID . '&payno=' . $this->user['username'] . '&typ=2&money=' . $money . '&back_url=' . $back_url;
            return $url;
            //mcbpay
        }
        if ($post['pay_type'] == 'mcb_qqpay') {

            //mcbpay
            include($_SERVER["DOCUMENT_ROOT"] . '/mcbpay/mcbconfig.php');
            $back_url = 'http://' . $_SERVER['HTTP_HOST'] . '/index/mcbpay/back_url';
            $back_url = urlencode($back_url);
            $money = $post['bpprice'] * MCB_beilv;
            $url = "http://" . MCB_PAYDOMAIN . '/pay/pay.php?appid=' . MCB_APPID . '&payno=' . $this->user['username'] . '&typ=4&money=' . $money . '&back_url=' . $back_url;
            return $url;
            //mcbpay
        }
        //易达方
        if (strpos($post['pay_type'], 'edf_') !== false) {
            $r = explode("_", $post['pay_type']);
            $bankcode = $r[1];
            $url = "/extend/edfpay/pay.php?money=" . $money . "&orderid=" . $orderid . '&bankid=' . $bankcode;
            return $url;
        }
        //趣聚合
        if (strpos($post['pay_type'], 'qujh_') !== false) {
            $r = explode("_", $post['pay_type']);
            $bankcode = $r[1];
            $url = "/extend/qujuhepay/pay.php?money=" . $money . "&orderid=" . $orderid . '&bankid=' . $bankcode;
            return $url;
        }

        //诺威
        if (strpos($post['pay_type'], 'nuow_') !== false) {
            $r = explode("_", $post['pay_type']);
            $bankcode = $r[1];
            $url = "/extend/nuoweipay/pay.php?money=" . $money . "&orderid=" . $orderid . '&bankid=' . $bankcode;
            return $url;
        }


        //xxx微信支付
        if ($post['pay_type'] == 'wx_wap_2') {
            $res = $Pay->wx_wap_2($data);

            return $res;
        }


        //秒冲宝
        if (in_array($post['pay_type'], array('mcpay'))) {
            $res = $Pay->mcpay($data);

            return $res;
        }

        //evepay
        if (in_array($post['pay_type'], array('eve_alih5', 'eve_wxh5', 'eve_alicode', 'eve_wxcode', 'eve_kuaijie'))) {

            switch ($post['pay_type']) {
                case 'eve_alih5':
                    $code = 2;
                    break;
                case 'eve_wxh5':
                    $code = 3;
                    break;
                case 'eve_alicode':
                    $code = 4;
                    break;
                case 'eve_wxcode':
                    $code = 5;
                    break;
                case 'eve_kuaijie':
                    $code = 6;
                    break;

                default:
                    return false;
                    break;
            }
            $res = $Pay->evepay($data, $code);

            return $res;
        }

        //鑫支付
        if ($post['pay_type'] == 'xinpay_wx') {
            $xinpay = new \app\common\library\Xinpay();
            $result = $xinpay->createOrder($data);
            if ($result && isset($result['bill_no'])) {
                $_edit = array();
                $_edit['bpid'] = $data['bpid'];
                $_edit['balance_sn'] = $result['bill_no'];//单号和平台单号一致
                $is_edit = db('balance')->update($_edit);
                if ($is_edit) {
                    $url = "/index/user/chongzhi.html?pay_type=xinpay_wx&bill_no={$result['bill_no']}&bill_fee={$money}&pay_link={$result['pay_link']}";
                    return $url;
                }
            }
            return WPreturn('支付失败，请刷新页面重试！', -1);
        }

        //接入fastpay
        if ($post['pay_type'] == 'fastpay_wx') {
            if (!function_exists('get_openid')) {
                require $_SERVER['DOCUMENT_ROOT'] . '/fastpay/Fast_Cofig.php';
                $paydata = array();
                $paydata['uid'] = $uid;//支付用户id
                $paydata['order_no'] = $orderid;//订单号
                $paydata['total_fee'] = $money;//金额
                $paydata['param'] = "";//其他参数
                $paydata['pay_title'] = "会员充值";//
                $paydata['me_back_url'] = Request::instance()->domain() . '/index/user/index';//支付成功返回的地址
                $geturl = fastpay_order($paydata);//获取支付链接
                return $geturl;
            }
        }


    }


    /**
     * 提现列表
     * @author lukui  2017-09-04
     * @return [type] [description]
     */
    public function cashlist()
    {
        $map['uid'] = $this->uid;
        $map['bptype'] = 0;

        $list = db('balance')->where($map)->order('bpid desc')->limit(100)->select();

        $this->assign('list', $list);

        return $this->fetch();
    }

    public function myteam()
    {
        $myoids = myoidswithout101($this->uid,array());
        $this->assign('id', $this->uid);

        $this->assign('myoids', $myoids?count($myoids):0);
        return $this->fetch();
    }

    public function fenrunlist()
    {
        $list = db('price_log p, wp_order o')->where('p.oid = o.oid')->field('o.fee,p.account,p.time')
            ->where('p.uid','=',$this->uid)->where('p.title','=','分润')->order('p.id','desc')->select();

        $this->assign('list', $list);

        return $this->fetch();
    }


    /**
     * 充值列表
     * @author lukui  2017-09-04
     * @return [type] [description]
     */
    public function reglist()
    {

        $map['uid'] = $this->uid;
        $map['bptype'] = 1;

        $list = db('balance')->where($map)->order('bpid desc')->limit(100)->select();

        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 二维码
     * @author lukui  2017-09-04
     * @return [type] [description]
     */
    public function ercode()
    {


//        $user = $this->user;

        //推广二维码
//        if ($user['otype'] == 101) {
//            $oid = $this->uid;
//        } else {
//            $oid = $user['oid'];
//        }
        $oid = $this->uid;
        $oid_url = "http://" . $_SERVER['HTTP_HOST'] . '?fid=' . $oid;
        $oid_url = urlToShort($oid_url);
        $this->assign('oid_url', $oid_url);
        return $this->fetch();
    }

    public function mcpay()
    {


        $id = input('id');
        if (!$id) {
            $this->error('参数错误！');
        }

        $balance = db('balance')->where('bpid', $id)->find();
        if (!$balance) {
            $this->error('参数错误！');
        }
        $appid = "2017072346";//扫码应用APPID
        $username = $balance['balance_sn'];///调用网站前台登录的用户名;
        $back_url = 'http://' . $_SERVER['HTTP_HOST'] . '/index/pay/mcb_notify';//成功返回页面
        $back_url = urlencode($back_url);

        $this->assign('balance', $balance);
        $this->assign('appid', $appid);
        $this->assign('back_url', $back_url);
        $this->assign('username', $username);
        return $this->fetch();
    }

    /**
     * 第四方充值通道
     * @return false|mixed|string
     */
    public function chongzhi()
    {
        $pay_type = $_GET['pay_type'];

        if (!$pay_type && !in_array($pay_type, array('xinpay_wx'))) {
            $this->redirect('/index/user/index');
        }

        if ($pay_type == 'xinpay_wx') {
            $bill_no = $_GET['bill_no'];
            $pay_link = $_GET['pay_link'];
            $money = $_GET['bill_fee'];

            if (!$bill_no && !$pay_link && !$money) {
                $this->redirect('/index/user/index');
            }

            $ord = db('balance')->where('balance_sn', $bill_no)->find();
            if ($ord && !$ord['isverified'] && $ord['bptype'] == 3 && ($this->user['uid'] == $ord['uid'])) {
                $this->assign('pay_type', $pay_type);
                $this->assign('bill_no', $bill_no);
                $this->assign('money', $money);
                $this->assign('qrcode_url', 'https://pay.swiftpass.cn/pay/qrcode?uuid=' . urlencode($pay_link));
            } else {
                $this->redirect('/index/user/index');
            }
        }

        return $this->fetch();
    }

    /**
     * 校验订单的支付状态
     */
    public function check_order()
    {
        $bill_no = $_GET['bill_no'];
        $pay_type = $_GET['pay_type'];

        $where = array(
            'balance_sn' => $bill_no,
            'pay_type' => $pay_type,
        );
        $ord = db('balance')->where($where)->find();
        if (!$ord || ($this->user['uid'] != $ord['uid'])) {
            WPreturn('支付订单不存在！', -1, '/index/user/index');
        }

        if (!$ord['isverified'] && $ord['bptype'] == 3) {
            return WPreturn('', -1);
        }
        return WPreturn('支付成功！', 0, '/index/user/index');
    }

    /**
     * 获取提现用户的openid
     */
    public function get_pay_openid()
    {
        $pay_openid = isset($_COOKIE['pay_openid']) ? $_COOKIE['pay_openid'] : null;

        if ($pay_openid) {
            $this->redirect('/index/user/index?id=tixian');
        }

        if (!function_exists('get_openid')) {
            require $_SERVER['DOCUMENT_ROOT'] . '/fastpay/Fast_Cofig.php';
        }

        if (empty($pay_openid)) {
            $pay_openid = get_openid();
            $time_out = time() + 3600 * 24 * 6;//6天后过期
            $_COOKIE['pay_openid'] = $pay_openid;
            setcookie("pay_openid", $pay_openid, $time_out, "/");
            Log::log('获取openid:' . $pay_openid);
            $this->redirect('/index/user/index?id=tixian');
        }
    }


}
